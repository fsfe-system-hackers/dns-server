# DNS Server

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/dns-server/00_README)

Ansible playbook to setup DNS server. No DNS zone data are set up.
