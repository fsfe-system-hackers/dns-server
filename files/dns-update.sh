#!/bin/bash

## Script to update DNS zone files from Git
## SPDX-License-Identifier: GPL-3.0-or-later
## SPDX-FileCopyrightText: 2019 Free Software Foundation Europe e.V.

## Some parameters

# Path that will be updated from Git
GITPATH=/etc/bind

# Notify address in case of errors
NOTIFYLIST="system-hackers@lists.fsfe.org"

# Temporary files catching stdout of git pull and named-checkconf
GIT_OUTFILE=$(mktemp)
CHECK_OUTFILE=$(mktemp)

# Files that should be removed before exiting
DELTEMPFILES="$GIT_OUTFILE $CHECK_OUTFILE"

## End of parameter section


# We want English output
export LANG="en_US.UTF-8"

# Hostname for error messages
CURHOST=$(hostname -f)

# Helpers
verbose=0
while getopts "hv" opt; do
  case "$opt" in
  h)  echo "This script updates DNS zone files from Git"
      echo "Add -v as parameter to print more output"
      exit 0
      ;;
  v)  verbose=1
      ;;
  *)  echo "Invalid flag"
      exit 1
      ;;
  esac
done

function cleanuptempfiles() {
  for delfile in $DELTEMPFILES; do
    rm -f "${delfile}"
  done
}

function debug() {
  if [[ $verbose == 1 ]]; then
    tee -a "$@"
  else
    cat >> "$@"
  fi
}

# Go into GITPATH (-C is not available in old Git versions)
cd "${GITPATH}" || exit 1

# Update files from GIT
echo "Pulling from Git"
i=0
EXIT_GIT=1
while [[ ( $EXIT_GIT -ne 0 ) && ( $i -lt 3) ]]; do
  ((i++))

  git pull origin master 2>&1 | debug "${GIT_OUTFILE}"
  # Exit code of first command in pipe (pull)
  EXIT_GIT=${PIPESTATUS[0]}

  if [[ $EXIT_GIT != 0 ]]; then
    (echo "Error. Trying again in a few seconds"; echo "---") | debug "${GIT_OUTFILE}"
    sleep "$(shuf -i 10-30 -n1)"
  fi
done

if [[ $EXIT_GIT != 0 ]]; then
  echo -e "
  git pull produced the following error message when
  trying to update the DNS configuration files:

------------------------------------------------------------------------
$(cat "${GIT_OUTFILE}")
------------------------------------------------------------------------

  Please check what's wrong and fix the cause of the problem.

  " | mail -s "${CURHOST}: DNS config update error (git)" $NOTIFYLIST

  # remove temporary files and exit
  cleanuptempfiles
  exit 1
fi

# Get latest commit hash
GITREV=$(git rev-parse --short=10 HEAD)

# Test DNS config
echo -e "\nChecking DNS config"
named-checkconf -z "${GITPATH}"/named.conf 2>&1 | debug "${CHECK_OUTFILE}"

# Exit code of first command in pipe (named-checkconf)
EXIT_CHECK=${PIPESTATUS[0]}

if [[ $EXIT_CHECK != 0 ]]; then
  echo -e "
  named-checkconf produced the following error message when
  checking the DNS configuration files at revision ${GITREV}:

------------------------------------------------------------------------
$(cat "${CHECK_OUTFILE}")
------------------------------------------------------------------------

  Please check what's wrong and fix the cause of the problem.

  " | mail -s "${CURHOST}: DNS config update error (bind)" $NOTIFYLIST

  # remove temporary files and exit
  cleanuptempfiles
  exit 1
fi

# Fix bind permissions
chown -R root:bind "${GITPATH}"
find "${GITPATH}"/zones -type d -print0 | xargs -0 chmod g+s
chown bind:bind "${GITPATH}"/rndc.key
chmod 640 "${GITPATH}"/rndc.key

# Reload bind config if something new has been pulled
if ! head -n1 "${GIT_OUTFILE}" | grep -qE "^Already up[ -]to[ -]date"; then
  echo -e "\nUpdated DNS config to ${GITREV}"
  /usr/sbin/service bind9 reload
else
  echo -e "\nNo new files, did not reload bind (${GITREV})"
fi

# Delete temporary files
cleanuptempfiles

exit 0
